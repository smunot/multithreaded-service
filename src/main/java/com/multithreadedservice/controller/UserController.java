package com.multithreadedservice.controller;

import com.multithreadedservice.entity.User;
import com.multithreadedservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping(value = "/users", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE}, produces = "application/json")
    public ResponseEntity saveFile(@RequestParam(value = "files") MultipartFile[] files) {
        for (MultipartFile file : files) {
            userService.saveData(file);
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping(value = "/users", produces = "application/json")
    public CompletableFuture<ResponseEntity> getUsers() {
        return userService.getUsers().thenApply(ResponseEntity::ok);
    }

    @GetMapping(value = "/usersByThread", produces = "application/json")
    public ResponseEntity usersByThread() {
        CompletableFuture<List<User>> user1 = userService.getUsers();
        CompletableFuture<List<User>> user2 = userService.getUsers();
        CompletableFuture<List<User>> user3 = userService.getUsers();
        CompletableFuture.allOf(user1,user2,user3).join();
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    @GetMapping(value = "/getHeader", produces = "application/json")
    public ResponseEntity getHeader(@RequestHeader Map<String,String> headers) {
        for(Map.Entry<String,String> e: headers.entrySet()){
            System.out.println(e.getKey()+" = "+e.getValue());
        }
        return ResponseEntity.ok().build();
    }
}
