package com.multithreadedservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultithreadedServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultithreadedServiceApplication.class, args);
	}

}
