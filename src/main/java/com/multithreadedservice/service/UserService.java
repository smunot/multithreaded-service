package com.multithreadedservice.service;

import com.multithreadedservice.entity.User;
import com.multithreadedservice.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class UserService {
    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Async
    public CompletableFuture<List<User>> saveData(MultipartFile file) {
        long start = System.currentTimeMillis();
        List<User> users = ParsCSVFile(file);
        LOGGER.info("Saving file of size {}", users.size(), "Thread" + Thread.currentThread().getName());
        users = userRepository.saveAll(users);
        long end = System.currentTimeMillis();
        LOGGER.info("Total Time {}", (end - start));
        return CompletableFuture.completedFuture(users);
    }

    @Async
    public CompletableFuture<List<User>> getUsers() {
        LOGGER.info("get list of ussers by {}" + Thread.currentThread().getName());
        List<User> users = userRepository.findAll();
        return CompletableFuture.completedFuture(users);
    }

    @Async
    private List<User> ParsCSVFile(MultipartFile file) {
        final List<User> users = new ArrayList<>();
        try (final BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            String line;
            while ((line = br.readLine()) != null) {
                final String[] data = line.split(",");
                User user = new User();
                user.setName(data[0]);
                user.setEmail(data[1]);
                user.setGender(data[2]);
                users.add(user);
            }
        } catch (Exception e) {
            LOGGER.error("failed with Exception");
            e.printStackTrace();
        }
        return users;
    }

}
